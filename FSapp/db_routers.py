from accounts.models import UserLogs


class LogsRouter:
    def db_for_write(self, model, **hints):
        if model == UserLogs:
            return 'logs'
        return None
