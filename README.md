# Run via Docker
    docker-compose up -d --build
### Logs
    docker-compose logs -f

# Run manually
    sudo apt install redis-server
    redis-server
    pip3 install -r requirements.txt
    python3 manage.py runserver
    celery -A FSapp worker -l info
    celery -A FSapp beat -l info