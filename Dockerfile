FROM python:3.7

ENV PYTHONUNBUFFERED 1
ENV DJANGO_ENV dev
ENV DOCKER_CONTAINER 1
ENV APP_ROOT /code

RUN mkdir ${APP_ROOT}
COPY . ${APP_ROOT}
WORKDIR ${APP_ROOT}

RUN pip install --upgrade pip
RUN pip install -r ${APP_ROOT}/requirements.txt

EXPOSE 8000