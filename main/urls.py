from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views

app_name = 'main'

urlpatterns = [
    path('', login_required(views.home), name='home'),
    path('upload/', login_required(views.upload), name='upload'),
    path('download/<int:id>', login_required(views.download), name='download'),
    path('share/<int:id>', login_required(views.share), name='share'),
    path('comments/<int:id>', login_required(views.comments), name='comments'),
    path('comments/delete/<int:id>', login_required(views.delete_comment), name='delete_comment'),
    path('comments/update/<int:id>', login_required(views.update_comment), name='update_comment'),
]