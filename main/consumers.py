import asyncio
import html
import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from .models import Files, Comments, FilePermissions


class CommentConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.user = self.scope['user']
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # TODO: AuthMiddleware olduğuna görə user.is_authenticated-a ehtiyyac yoxdu deyəsən.
        #  Dəqiqləşdir, lazım deyilsə sil.
        if not self.user.is_authenticated or not await self.authenticate():
            return

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        # xss protection
        message = html.escape(text_data_json['message'].strip())

        # save to database without waiting
        loop = asyncio.get_event_loop()
        loop.create_task(self.save_message(message))

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message,
                'sender': self.user.username
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        user = event['sender']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'user': user
        }))

    @database_sync_to_async
    def authenticate(self):
        """file & permission validation"""

        # check does file exist
        try:
            file = Files.objects.get(id=self.room_name)
            # if file doesn't belong to user then check permission
            if file.owner != self.user:
                try:
                    FilePermissions.objects.get(file=file, user=self.user, allow_comments=True)
                    return True
                except FilePermissions.DoesNotExist:
                    return False
            else:  # if file belongs to user
                return True
        except Files.DoesNotExist:
            return False

    @database_sync_to_async
    def save_message(self, message):
        Comments.objects.create(text=message, file_id=self.room_name, user=self.user)
