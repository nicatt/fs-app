from django.urls import re_path, path

from .consumers import CommentConsumer


websocket_urlpatterns = [
    re_path(r'ws/comments/(?P<room_name>\d+)/$', CommentConsumer)
]