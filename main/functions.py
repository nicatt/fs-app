import datetime
import os
import zipfile
from django.core.files.storage import Storage

from FSapp.settings import BASE_DIR


def handle_uploaded_file(file, file_name):
    # if there is an unexpected symbol in file name, Django changes name of file
    # for example: 'yeni shekil®.jpg' -> yeni_shekil.jpg
    # we should change it by ourselves in advance
    storage = Storage()
    today = datetime.date.today()
    folder = today.strftime('/%Y/%m/%d/')
    path = BASE_DIR + '/media/uploads' + folder
    file_path = path + storage.get_valid_name(file.name)

    # upload file
    with open(file_path, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)

    # add file into zip
    zil_file = zipfile.ZipFile(path + 'File-'+str(file_name)+'.zip', 'w')
    zil_file.write(file_path, os.path.basename(file_path), compress_type=zipfile.ZIP_DEFLATED)
    zil_file.close()

    # delete file
    os.remove(file_path)
