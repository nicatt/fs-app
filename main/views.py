import html
import os

from django.contrib.auth.models import User
from django.db.models import Q
from django.db.utils import IntegrityError
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404, HttpResponse, Http404

from FSapp.settings import MEDIA_ROOT
from .forms import FileUploadForm, FileShareForm
from .functions import handle_uploaded_file
from .models import Files, FilePermissions, Comments


def home(request):
    my_files = Files.objects.filter(owner=request.user.id)
    shared = FilePermissions.objects.filter(user=request.user)

    return render(request, 'account/dashboard.html', {'my_files': my_files, 'shared': shared})


def upload(request):
    # TODO: VirusTotalın API-ını araşdır. Əgər yoxlamaq olursa faylın viruslu olub-olmamasını yoxla
    if request.POST and request.FILES:
        form = FileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file_obj = form.save(commit=False)
            file_obj.owner = request.user
            file_obj.save()

            handle_uploaded_file(request.FILES['file'], file_obj.id)

            return redirect('main:home')
    else:
        form = FileUploadForm()

    return render(request, 'account/upload.html', {'form': form})


def download(request, id):
    # is this file belongs to me
    try:
        file = Files.objects.get(id=id, owner=request.user)
    except Files.DoesNotExist:
        # do i have a permission to download this file
        permission = get_object_or_404(FilePermissions, Q(file_id=id, user=request.user))
        file = permission.file

    folder = file.upload_date.strftime('%Y/%m/%d')
    file_path = MEDIA_ROOT + '/uploads/' + folder + '/File-' + str(file.id) + '.zip'

    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/zip")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404


# TODO: səliqə sahmana sal
def share(request, id):
    file = get_object_or_404(Files, Q(owner=request.user), pk=id)

    if request.POST:
        form = FileShareForm(request.POST)
        if form.is_valid():
            try:
                user = User.objects.get(username=form.cleaned_data.get('username'))
            except User.DoesNotExist:  # if user doesn't exist
                return render(request, 'account/share.html', {'form': form, 'errors': 'User does not exists.'})

            if user == request.user:  # if username is same with own username
                return render(request, 'account/share.html', {
                    'form': form, 'errors': 'You can not give an access to yourself.'
                })

            # FIXME: əgər kiməsə access veribsə ikinci dəfə girəndə
            #  allow_commentsi-i dəyişə bilməyəcək. (update lazımdır)
            try:
                form_obj = form.save(commit=False)
                form_obj.file = file
                form_obj.user = user
                form.save()
            except IntegrityError:
                # see FilePermissions -> unique_together()
                return render(request, 'account/share.html', {
                    'form': form, 'errors': 'This user has already permission.'
                })

            return redirect('main:home')
    else:
        form = FileShareForm()

    return render(request, 'account/share.html', {'form': form, 'errors': None})


def comments(request, id):
    """Only file owners and allowed users can comment"""

    # check does file exist
    file = get_object_or_404(Files, pk=id)

    # if file doesn't belong to user then check permission
    if file.owner != request.user:
        get_object_or_404(FilePermissions, Q(file=file, user=request.user, allow_comments=True))

    last_comments = Comments.objects.filter(file=file).order_by('date').all()

    return render(request, 'account/comments.html', {
        'file_id': id,
        'last_comments': last_comments,
        'user': request.user
    })


def delete_comment(request, id):
    # TODO: şərhi siləndən sonra digər isifadəçilər ancaq səhifə refresh olandan sonra biləcək
    try:
        # if user is admin
        if request.user.is_superuser:
            Comments.objects.get(id=id).delete()
            status = True
        else:
            # if comment belongs to user
            Comments.objects.get(id=id, user=request.user).delete()
            status = True
    except Comments.DoesNotExist:
        status = False

    return JsonResponse({'status': status})


def update_comment(request, id):
    if request.POST and request.POST['text']:
        try:
            text = html.escape(request.POST['text'])
            comment = Comments.objects.get(id=id, user=request.user)
            comment.text = text
            comment.save()
            status = True
        except Comments.DoesNotExist:
            status = False
    else:
        status = False
    return JsonResponse({'status': status})
