import os
import shutil
from datetime import datetime, timedelta

from celery import shared_task
from django.utils import timezone

from FSapp.settings import MEDIA_ROOT
from .models import Files


@shared_task
def delete_old_files():
    # deleting files on database
    Files.objects.filter(upload_date__lte=timezone.now() - timedelta(days=7)).delete()

    # deleting files on server
    a_week_ago = datetime.today() - timedelta(days=7)
    folder = a_week_ago.strftime('uploads/%Y/%m/%d')
    path = os.path.join(MEDIA_ROOT, folder)
    if os.path.isdir(path):
        shutil.rmtree(path)
