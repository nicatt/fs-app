from django.db import models
from django.contrib.auth.models import User


class Files(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.TextField(max_length=1000)
    file = models.FileField(null=True, upload_to="uploads/%Y/%m/%d")
    upload_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.description[0:10]

    class Meta:
        verbose_name = 'File'
        verbose_name_plural = 'Files'


class FilePermissions(models.Model):
    file = models.ForeignKey(Files, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    allow_comments = models.BooleanField(default=False)

    class Meta:
        db_table = 'main_file_permissions'
        # prevent giving access to same user
        unique_together = ('file', 'user')

    def __str__(self):
        return self.file.description[0:10]


class Comments(models.Model):
    text = models.CharField(max_length=255)
    file = models.ForeignKey(Files, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
