from django import forms
from .models import Files, FilePermissions


class FileUploadForm(forms.ModelForm):
    class Meta:
        model = Files
        fields = (
            "description",
            "file"
        )


class FileShareForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput, max_length=150)
    # allow_comments = forms.BooleanField(widget=forms.CheckboxInput, required=False)

    class Meta:
        model = FilePermissions
        fields = (
            # 'username',
            'allow_comments',
        )
