from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from accounts.decorators import logout_required
from accounts.models import UserLogs

from .forms import LoginForm


@logout_required
def signup_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()

            user_name = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')

            user = authenticate(username=user_name, password=password)
            login(request, user)

            return redirect('main:home')
    else:
        form = UserCreationForm()

    return render(request, 'registration/signup.html', {'form': form})


@logout_required
def login_view(request):
    if request.POST:
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']

            user = authenticate(username=username, password=password)
            if user is not None and user.is_active:
                login(request, user)

                x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
                if x_forwarded_for:
                    ip = x_forwarded_for.split(',')[0]
                else:
                    ip = request.META.get('REMOTE_ADDR')

                user_agent = request.META['HTTP_USER_AGENT']

                # store user logs
                logs = UserLogs(user_id=user.id, user_agent=user_agent, ip=ip)
                logs.save()

                return redirect('main:home')

        # if user is not exists or is not active
        return render(request, 'registration/login.html', {
            'form': LoginForm(),
            'error': 'Wrong username or password'
        })

    else:
        return render(request, 'registration/login.html', {'form': LoginForm(), 'error': None})


@login_required
def logout_view(request):
    logout(request)
    return redirect('main:home')
