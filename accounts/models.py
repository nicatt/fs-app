from django.db import models


class UserLogs(models.Model):
    user_id = models.BigIntegerField()
    user_agent = models.CharField(max_length=255)
    ip = models.CharField(max_length=60)
